import React from 'react';
import {View, Text} from 'react-native';

import {
  Onfido,
  OnfidoCaptureType,
  OnfidoCountryCode,
  OnfidoDocumentType,
} from '@onfido/react-native-sdk';

const apiOnfido = 'https://api.onfido.com/v3/';
const apiToken =
  'Token token=api_sandbox.fN9XcElmSz6.j_2JgquvdztzSE40NeGDk23-YtAyqJ3k';
const onFidoDocs = [
  ['Passport', OnfidoDocumentType.PASSPORT, 'passport'],
  ["Driver's License", OnfidoDocumentType.DRIVING_LICENCE, 'car'],
  [
    'National Identity Card',
    OnfidoDocumentType.NATIONAL_IDENTITY_CARD,
    'account-card-details-outline',
  ],
  ['Residence Permit', OnfidoDocumentType.RESIDENCE_PERMIT, 'home-account'],
  ['Visa', OnfidoDocumentType.VISA, 'passport'],
  [
    'Work Permit',
    OnfidoDocumentType.WORK_PERMIT,
    'account-card-details-outline',
  ],
  ['STA Application', OnfidoDocumentType.GENERIC, 'shield-lock-outline'],
  [
    'Document Verification',
    OnfidoDocumentType.GENERIC,
    'file-document-outline',
  ],
  ['Facial Recognition', OnfidoDocumentType.GENERIC, 'account-outline'],
  ['Generic', OnfidoDocumentType.GENERIC, 'shield-lock-outline'],
];

var userOnFido;
var selectedDocument;
var selectedCountry = OnfidoCountryCode.USA;
var selfieType;

export default class OnFidoApi {

  static onFidoDocs() {
    return onFidoDocs;
  }

  static async createOnFido(firstName, lastName, email) {
    var data = JSON.stringify({
      first_name: firstName,
      last_name: lastName,
      email: email,
    });

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          resolve(JSON.parse(this.responseText));
        }
      });

      xhr.open('POST', apiOnfido + 'applicants/');
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', apiToken);

      xhr.send(data);
    });
  }

  static async deleteOnFido(deleteid) {
    var data = JSON.stringify(false);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log('ONFIDO USER DELETED');
          console.log(this.responseText);
          resolve();
        }
      });

      xhr.open('DELETE', apiOnfido + 'applicants/' + deleteid);
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', apiToken);

      xhr.send(data);
    });
  }

  static async downloadOnFidoDoc(documentId) {
    var data = JSON.stringify(false);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          var arraybuffer = xhr.response;
          var headers = xhr.getAllResponseHeaders();
          var content = xhr.getResponseHeader('content-type');
          console.log('RESPONSE BYTE ' + arraybuffer);
          //console.log("HEADERS "+headers);
          console.log('CONTENT ' + content);

          resolve(arraybuffer);
        }
      });

      xhr.open('GET', apiOnfido + 'documents/' + documentId + '/download');
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', apiToken);
      xhr.responseType = 'blob';

      xhr.send(data);
    });
  }

  static async downloadDocumentOnfido(filename) {
    console.log('DOCUMENT TEST DOWNLOAD ');
    await fetch(apiOnfido + 'documents/' + filename + '/download', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        authorization: apiToken,
      },
      body: false,
    })
      .then((response) => {
        console.log(response);
        return new Promise((resolve, reject) => {
          resolve(response.blob());
        });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  static async downloadOnFidoFace(documentId) {
    var data = JSON.stringify(false);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          resolve(this.response);
        }
      });

      xhr.open('GET', apiOnfido + 'live_photos/' + documentId + '/download');
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', apiToken);

      xhr.send(data);
    });
  }

  static async getOnFidoDoc(documentId) {
    var data = null;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          resolve(JSON.parse(this.responseText));
        }
      });

      xhr.open('GET', apiOnfido + 'documents/' + documentId);
      xhr.setRequestHeader('authorization', apiToken);

      xhr.send(data);
    });
  }

  static async onfidoPass(
    documentType,
    countryCode,
    userOnFidoId,
    withSelfie,
    photoSelfie,
  ) {
    userOnFido = userOnFidoId;
    if (documentType == 'passport') {
      selectedDocument = OnfidoDocumentType.PASSPORT;
    } else if (documentType == 'nationalIdentityCard') {
      selectedDocument = OnfidoDocumentType.NATIONAL_IDENTITY_CARD;
    } else if (documentType == 'drivingLicence') {
      selectedDocument = OnfidoDocumentType.DRIVING_LICENCE;
    } else {
      selectedDocument = OnfidoDocumentType.GENERIC;
    }

    if (withSelfie) {
      if (photoSelfie) {
        selfieType = OnfidoCaptureType.PHOTO;
      } else {
        selfieType = OnfidoCaptureType.VIDEO;
      }
    }

    var data = JSON.stringify({
      applicant_id: userOnFido,
      application_id: 'com.goverify',
    });

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          var auxToken = JSON.parse(this.responseText).token;
          console.log(auxToken);

          if (withSelfie) {
            Onfido.start({
              sdkToken: auxToken,
              flowSteps: {
                welcome: false,
                captureFace: {
                  type: selfieType,
                },
                captureDocument: {
                  docType: selectedDocument,
                  countryCode: selectedCountry,
                },
              },
            })
              .then((res) => {
                this.onfidoSuccess(JSON.stringify(res));
                resolve(res);
              })
              .catch((err) =>
                console.warn('OnfidoSDK: Error:', err.code, err.message),
              );
          } else {
            Onfido.start({
              sdkToken: auxToken,
              flowSteps: {
                welcome: false,
                captureDocument: {
                  docType: selectedDocument,
                  countryCode: selectedCountry,
                },
              },
            })
              .then((res) => {
                //this.onfidoSuccess(JSON.stringify(res));
                resolve(res);
              })
              .catch((err) =>
                console.warn('OnfidoSDK: Error:', err.code, err.message),
              );
          }
        }
      });

      xhr.open('POST', apiOnfido + 'sdk_token');
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', apiToken);

      xhr.send(data);
    });
  }

  static async onfidoSuccess(onFidoRes) {
    console.log('OnfidoSDK: Success:' + onFidoRes);
    return new Promise((resolve, reject) => {
      resolve();
    });
  }
}
