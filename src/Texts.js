import React from 'react';
import {View, Text} from 'react-native';

const privacyPolicy = [
  'The "Company" has engaged Goverify, Inc. to obtain a consumer report and/or investigative consumer report, for employment purposes. Goverify, Inc. will provide a background investigation as a pre-condition of your engagement with the Company and in compliance with applicable law. After you have completed the form, you can check the status of your background check on the "Track Verifications" section.',
  'I hereby agree to Goverify, Inc. Privacy Policy.',
];

const termsConditions1 = [
  'Rights Under FCRA',
  'Summary',
  'The Federal Fair Credit Reporting Act (FCRA) promotes the accuracy, fairness and privacy of information in the files of consumer reporting agencies. There are many types of consumer reporting agencies, including credit bureaus and speciality agencies (such as agencies that sell information about check writing histories, medical records, and rental history records). Here is a summary of your major rights under the FCRA. For more information, including information about additional rights, go to www.consumerfinance.gov/learnmore or write to: Consumer Financial Protection Bureau, 1700 G Street N.W., Washinghton, DC 20552. You must be told if information in your file has been used against you. Anyone who uses a credit report or another type of consumer report to deny your application for credit, insurance, or employment - or to take another adverse action against you - must tell you, and must give you the name, address, and phone number of the agency that provided the information. You have the right to know what is in your file. You may request and obtain all the information about you in the files of a consumer reporting agency (your "file disclosure").You will be required to provide proper identification, which may include your Social Security number. In many cases, the disclosure will be free. You are entitled to a free file disclosure if: - a person has taken adverse action against you because of information in your credit report; - you are the victim of identity theft and place a fraud alert in your file; - your file contains innacurate information as a result of fraud; - you are on public assistance; - you are unemployed but expect to apply for employment within 60 days.',
  'I acknowledge receipt of the Summary of Your Rights Under the Fair Credit Reporting Act (FCRA) and certify that I have read and understand this document.',
];
const termsConditions2 = [
  'Background Investigation',
  'Disclosure',
  'The "Company" may obtain information about you from a third party consumer reporting agency for employment purposes. Thus, you may be the subject of a "consumer report" and/or an "investigative consumer report", as defined in USA, which may include information about your character, general reputation, personal characteristics, and/or mode of living, and which can involve personal interviews with sources such as your neighbors, friends, or associates. These reports may contain information regarding your criminal history, motor vehicle records ("driving records"), verification of your education or employment history, or other background checks You have the right, upon written request made with a reasonable time, to request whether a consumer report has been run about you, and disclosure of the nature and scope of any investigative consumer report and to request a copy of your report. Please be advised that the nature and scope of the most common form of investigative consumer report is an employment history or verification. These searches will be conducted by Goverify, Inc., (ADDRESS), (PHONE), https://www.goverify.co. The scope of this disclosure is all-encompassing, however, allowing the Company to obtain from any outside organization all manner of consumer reports throughout the course of your employment to the extent permitted by law.',
  'I acknowledge receipt of the Disclosure Regarding Background Investigation policy and certify that I have read and understand this document.',
];
const termsConditions3 = [
  'Background Check',
  'Acknowledgement and Authorization',
  'I acknowledge receipt of the separate documents entitled Disclosure Regarding Background Investigation and a Summary of Your Rights Under the Fair Credit Reporting Act and certify that I have read and understand both of those documents. I hereby authorize the obtaining of "consumer reports" and/or "investigative consumer reports" by the Company at any time after receipt of this authorization and throughout my employment, if applicable and to the extent permitted by law. For the purpose of of preparing a background check for Company, I hereby authorize any law enforcement agency, administrator state or federal agency, institution, school or university (public or private), information service bureau, past or present employers, motor vehicle records agencies, or insurance company to furnish any and all background information requested by Goverify, Inc., (ADDRESS), (PHONE), https://www.goverify.co, as allowed by law, including information concerning my employment and earnings history, education, credit history, motor vehicle history, criminal history, military service, and professional credentials and licences. I agree that an electronic copy of this Authorization shall be as valid as the original.',
  'I consent to the execution of background checks over my data and indicate my agreement to all of the above.',
];

export default class Texts {
  static privacyPolicy() {
    return privacyPolicy;
  }

  static termsConditions1() {
    return termsConditions1;
  }

  static termsConditions2() {
    return termsConditions2;
  }

  static termsConditions3() {
    return termsConditions3;
  }
}
