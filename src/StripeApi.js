import React from 'react';
import {View, Text} from 'react-native';

const apiStripe = 'https://api.stripe.com/v1/';
const apiKey =
  'sk_test_51GqhgJLBxZXIvMitXyYZ4hViv3sKdpTRBcUPHQx1IhOm1LC8c8LBm4m4SOjSZuPQ6KsJWT4xlObpa3UUONBpty0b00O9qP3AxZ';

async function getPaymentMethod(cardData) {
  var expMonth = cardData.values.expiry.split('/')[0];
  var expYear = cardData.values.expiry.split('/')[1];
  var number = cardData.values.number;
  var cvc = cardData.values.cvc;

  var data =
    'type=card&card%5Bnumber%5D=' +
    number +
    '&card%5Bexp_month%5D=' +
    expMonth +
    '&card%5Bexp_year%5D=20' +
    expYear +
    '&card%5Bcvc%5D=' +
    cvc;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log(this.responseText);
        resolve(JSON.parse(this.responseText));
      }
    });

    xhr.open('POST', apiStripe + 'payment_methods');
    xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader('authorization', 'Bearer ' + apiKey);

    xhr.send(data);
  });
}

async function getPaymentIntent(amount, currency, paymentMethodId) {
  var data =
    'amount=' +
    amount +
    '&currency=' +
    currency +
    '&payment_method_types%5B%5D=card&confirm=true&payment_method=' +
    paymentMethodId;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log(this.responseText);
        resolve(JSON.parse(this.responseText));
      }
    });

    xhr.open('POST', apiStripe + 'payment_intents');
    xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader('authorization', 'Bearer ' + apiKey);

    xhr.send(data);
  });
}

export default class StripeApi {
  static async processPayment(cardData, amount, currency, plan, newPlanName) {
    var expMonth = cardData.values.expiry.split('/')[0];
    var expYear = cardData.values.expiry.split('/')[1];

    const cardPaymentMethod = await getPaymentMethod(cardData);
    //console.log(cardPaymentMethod);

    var paymentMethodId = cardPaymentMethod.id;
    
    const paymentIntent = await getPaymentIntent(
      amount * 100,
      currency,
      paymentMethodId,
    );

    console.log('PAYMENT INTENT');

    console.log('AMOUNT: ' + amount);
    console.log('CURRENCY: ' + currency);

    return new Promise((resolve, reject) => {
      resolve(paymentIntent);
    });


  }
}
