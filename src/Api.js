import React from 'react';
import {View, Text} from 'react-native';

const api = 'https://goverify-apissl.southcentralus.azurecontainer.io';

var userToken;
var catalogs;
var userData;
var userOnFido;
var userFullname;
var userInitials;
var userEmail;
var allPlans;
var allPlanTypes;
var userPlans;
var userPlanCount;

async function getCatalogs() {
  var data = null;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log('END READ CATALOGS');
        //console.log(this.responseText);
        catalogs = JSON.parse(this.responseText);
        resolve();
      }
    });

    xhr.open('GET', api + '/api/catalog');
    xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

    console.log('START READ CATALOGS');
    xhr.send(data);
  });
}

async function getUserData() {
  var data = null;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log('END GET USER DATA');

        userData = JSON.parse(this.responseText);

        try {
          userOnFido = userData.onfidoId;
        } catch {}
        userFullname = userData.fullName;
        userInitials = (
          userData.firstName.charAt(0) + userData.lastName1.charAt(0)
        ).toUpperCase();
        userEmail = userData.userId.email;

        resolve();
      }
    });

    xhr.open('GET', api + '/api/current-individual');
    xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

    console.log('START GET USER DATA');

    xhr.send(data);
  });
}

async function getPlans() {
  var data = null;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log('END READ PLANS');
        //console.log(this.responseText);
        allPlans = JSON.parse(this.responseText);
        resolve();
      }
    });

    xhr.open('GET', api + '/api/plan');
    xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

    console.log('START READ PLANS');
    xhr.send(data);
  });
}

async function getPlanTypes() {
  var data = null;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        //console.log(this.responseText);
        console.log('END READ PLAN TYPES');
        allPlanTypes = JSON.parse(this.responseText);
        resolve();
      }
    });

    xhr.open('GET', api + '/api/plan-type');
    xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

    console.log('START READ PLAN TYPES');
    xhr.send(data);
  });
}

async function getBoughtPlans() {
  var data = null;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log('END GET BOUGHT PLANS');
        userPlans = JSON.parse(this.responseText).rows;
        userPlanCount = userPlans.length;
        resolve();
      }
    });

    xhr.open('GET', api + '/api/bought-plan');
    xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

    console.log('START GET BOUGHT PLANS');
    xhr.send(data);
  });
}

async function signUp(username, password) {
  var data = JSON.stringify({
    username: username,
    email: username,
    password: password,
  });

  var xhr = new XMLHttpRequest();

  return new Promise((resolve, reject) => {
    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === this.DONE) {
        console.log(this.responseText);
        userToken = this.responseText;
        resolve(this.responseText);
      }
    });

    xhr.open('POST', api + '/api/auth/sign-up');
    xhr.setRequestHeader('content-type', 'application/json');

    xhr.send(data);
  });
}

export default class Api {
  static userToken() {
    return userToken;
  }

  static catalogs() {
    return catalogs;
  }

  static userData() {
    return userData;
  }

  static userInitials() {
    return userInitials;
  }

  static allPlans() {
    return allPlans;
  }

  static allPlanTypes() {
    return allPlanTypes;
  }

  static userPlans() {
    return userPlans;
  }

  static async login(username, password) {
    var data = JSON.stringify({
      email: username,
      password: password,
    });

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    console.log('START LOGIN');

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log('LOGIN ' + this.status);
          if (this.status == 200) {
            userToken = this.responseText;
            console.log('END LOGIN');
            resolve();
          } else {
            alert('Please Verify Your Credentials');
          }
        }
      });

      xhr.open('POST', api + '/api/auth/sign-in');
      xhr.setRequestHeader('content-type', 'application/json');

      xhr.send(data);
    });
  }

  static async steps() {
    await getCatalogs();
    await getUserData();
    await getPlans();
    await getPlanTypes();
    await getBoughtPlans();

    return new Promise((resolve, reject) => {
      resolve(userData);
    });
  }

  static async getPreRegistration(preRegisterToken) {
    var linkToken = preRegisterToken.split('=')[1];
    console.log('LINK TOKEN');
    console.log(linkToken);

    var data = JSON.stringify({
      token: linkToken,
    });

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log('END GET PRE REGISTRATION' + this.responseText);
          resolve(JSON.parse(this.responseText));
        }
      });

      xhr.open('POST', api + '/api/auth/verify-invitation');
      xhr.setRequestHeader('content-type', 'application/json');

      xhr.send(data);
    });
  }

  static async register(username, password) {
    await signUp(username, password);
    await getCatalogs();
  }

  static async getAuthUserId() {
    var data = null;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);

          resolve(JSON.parse(this.responseText).id);
        }
      });

      xhr.open('GET', api + '/api/auth/me');
      xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

      xhr.send(data);
    });
  }

  static async updateIndividual(firstName, lastName) {
    var data = JSON.stringify({
      id: userData.id,
      data: {
        onfidoId: userOnFido,
        privacyPolicyAck: true,
        rightsAck: true,
        disclosureAck: true,
        authorizationAck: true,
        lastName1: lastName,
        firstName: firstName,
      },
    });

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    console.log('START UPDATE');

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          resolve();
        }
      });

      xhr.open('PUT', api + '/api/individual/' + userData.id);
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

      xhr.send(data);
    });
  }

  static async createIndividual(userId, firstName, lastName) {
    var status = await this.getCatalogId('STS1', 'created');

    var data = JSON.stringify({
      data: {
        status: status,
        userId: userId,
        lastName1: firstName,
        firstName: lastName,
        onfidoId: userOnFido,
        privacyPolicyAck: true,
        rightsAck: true,
        disclosureAck: true,
        authorizationAck: true,
      },
    });

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    console.log('START CREATE INDIVIDUAL');

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          resolve();
        }
      });

      xhr.open('POST', api + '/api/individual/');
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

      xhr.send(data);
    });
  }

  static async getCatalogId(searchId, elementKey) {
    return new Promise((resolve, reject) => {
      catalogs.rows.map((catalog) => {
        if (catalog.catId == searchId) {
          catalog.values.map((verifierItem) => {
            if (verifierItem.key == elementKey) {
              console.log(
                'CATALOG: ' + catalog.catId + ' - ITEM: ' + verifierItem.key,
              );
              resolve(verifierItem.id);
            }
          });
        }
      });
    });
  }

  static async getVerification(searchId) {
    console.log("GET VERIFICATION");
    
    return new Promise((resolve, reject) => {
      allPlanTypes.rows.map((verType) => {
        
        try {
            console.log(verType);
            if (verType.verificationType.subType.key == searchId) {
              resolve(verType);
            }
        }catch(error){
            console.log("ERROR: "+error)
        }

      });
    });
  }

  static async setOnFido(onFidoId) {
    userOnFido = onFidoId;

    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  static async passwordReset() {
    console.log('PASSWORD RESET');

    //CHECK FUNCTION

    /** 
    var data = null;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);

          resolve();
        }
      });

      xhr.open('POST', api + '/api/auth/password-reset');
      xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

      xhr.send(data);
    });
    */
  }

  //TODO MODIFY DATA TO SEND
  static async createNewPlan(
    plan,
    documentSelected,
    amount,
    newPlanName,
    cardNumber,
    paymentIntentId,
  ) {
    var documentKey;

    if (documentSelected == 'passport') {
      documentKey = 'passport';
    } else if (documentSelected == 'nationalIdentityCard') {
      documentKey = 'national_identity_card';
    } else if (documentSelected == 'drivingLicence') {
      documentKey = 'driving_licence';
    } else {
      documentKey = 'driving_licence';
    }

    var verificationToPurchase = await this.getVerification(documentKey);
    var paymentMethodId = await this.getCatalogId('PME', 'credit_card');
    var statusCreated = await this.getCatalogId('STS1', 'created');
    var currencyId = await this.getCatalogId('CUR', 'usd');

    var data = JSON.stringify({
      data: {
        plan: verificationToPurchase.planId.id,
        verificationTypes: [verificationToPurchase.verificationType.id],
        paymentMethod: paymentMethodId,
        numberOfBoughtVerifications: 1,
        toPurchase: 1,
        price: amount,
        currency: currencyId,
        name: newPlanName,
        cardNumber: cardNumber,
        paymentIntent: paymentIntentId,
        status: statusCreated,
        assignedUsers: [userData.userId.id],
      },
    });

    console.log(data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    return new Promise((resolve, reject) => {
      xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          resolve();
        } else {
          console.log(this.responseText);
        }
      });

      xhr.open('POST', api + '/api/bought-plan');
      xhr.setRequestHeader('content-type', 'application/json');
      xhr.setRequestHeader('authorization', 'Bearer ' + userToken);

      xhr.send(data);
    });
  }
}
