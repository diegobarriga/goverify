/* eslint-disable react-native/no-inline-styles */
import 'react-native-gesture-handler';
import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Linking,
  ImageBackground,
  KeyboardAvoidingView,
  Image,
  Alert,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

//MyImports
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {
  TextInput,
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  Caption,
  Subheading,
  List,
  BottomNavigation,
  Text,
  Divider,
  IconButton,
  Menu,
  Dialog,
  Portal,
  Checkbox,
} from 'react-native-paper';
import Swiper from 'react-native-swiper';
import {
  CreditCardInput,
  LiteCreditCardInput,
} from 'react-native-credit-card-input';
import Api from './src/Api';
import OnFidoApi from './src/OnFidoApi';
import UUID from './src/UUID';
import Texts from './src/Texts';
import StripeApi from './src/StripeApi';

//TEMP IMPORT
import {
  Onfido,
  OnfidoCaptureType,
  OnfidoCountryCode,
  OnfidoDocumentType,
} from '@onfido/react-native-sdk';

//My Variables
var preRegisterToken;
var preRegisterUser;
var userData;

var newPlanName;
var documentSelected;
var selectSelfie = false;
var photoSelfie = true;
var onfidoRes;
var planPrice = 5;
var currency = 'USD';

//My Screens
function welcomeScreen({navigation}) {
  const {url: initialUrl, processing} = useInitialURL();

  preRegisterToken = initialUrl;
  console.log('PRE REGISTER TOKEN');
  console.log(preRegisterToken);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      <View
        style={{flex: 4, backgroundColor: 'white', justifyContent: 'center'}}>
        <Image
          style={{alignSelf: 'stretch', height: 300, width: 300}}
          source={require('./src/assets/images/Head_Logo_Text.png')}
        />
      </View>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Button
          style={{
            alignSelf: 'center',
            marginTop: 20,
          }}
          mode="contained"
          color="#e70065"
          onPress={async () => {
            navigation.navigate('Intro');
          }}>
          Get Started
        </Button>
        <Button
          style={{alignSelf: 'center', marginTop: 20}}
          mode="text"
          color="#e70065"
          onPress={async () => {
            navigation.navigate('Login');
          }}>
          Login
        </Button>
      </View>
    </View>
  );
}

function introScreen({navigation}) {
  const introStyles = StyleSheet.create({
    wrapper: {},
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'white',
    },
    image1: {
      flex: 1,
      resizeMode: 'contain',
      alignSelf: 'center',
    },
    text: {
      color: 'black',
      fontSize: 30,
      fontWeight: 'bold',
    },
    subtext: {
      color: 'black',
      fontSize: 20,
      fontWeight: 'normal',
    },
  });

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Swiper
        style={introStyles.wrapper}
        showsButtons={false}
        loop={false}
        showsPagination={false}
        autoplay={true}
        autoplayTimeout={1}
        activeDot={
          <View
            style={{
              backgroundColor: '#e70065',
              width: 8,
              height: 8,
              borderRadius: 4,
              marginLeft: 3,
              marginRight: 3,
              marginTop: 3,
              marginBottom: 3,
            }}
          />
        }
        dot={
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,.2)',
              width: 8,
              height: 8,
              borderRadius: 4,
              marginLeft: 3,
              marginRight: 3,
              marginTop: 3,
              marginBottom: 3,
            }}
          />
        }>
        <View style={introStyles.slide2}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'space-between',
                backgroundColor: 'white',
                flexDirection: 'column',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 100,
                  width: 350,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/GoVerify_Logo.png')}
              />
            </View>

            <View
              style={{
                flex: 5,
                alignItems: 'center',
                justifyContent: 'space-around',
                backgroundColor: 'white',
                flexDirection: 'column',
              }}>
              <Title
                style={{
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                Account Authentication
              </Title>

              <Image
                style={{
                  resizeMode: 'contain',
                  alignSelf: 'center',
                  height: 300,
                }}
                source={require('./src/assets/images/undraw_hiring_cyhs.png')}
              />
              <Paragraph
                style={{
                  alignSelf: 'center',
                  width: 250,
                  textAlign: 'center',
                  marginBottom: 30,
                }}>
                We can verify and authenticate your identity in less than 20
                seconds
              </Paragraph>
            </View>

            <View
              style={{
                flex: 1,

                backgroundColor: 'white',
              }}>
              <Button
                style={{
                  alignSelf: 'center',
                  width: 350,
                  marginBottom: 5,
                }}
                mode="contained"
                color="#e70065"
                onPress={async () => {}}>
                START
              </Button>
              <Checkbox.Item
                label="Accept Terms of Use"
                labelStyle={{
                  color: 'black',
                  fontSize: 12,
                }}
                status="checked"
                color="#e70065"
                style={{
                  width: 350,
                  height: 22,
                  alignSelf: 'center',
                }}
              />
              <Checkbox.Item
                label="Accept Privacy Policy"
                labelStyle={{
                  color: 'black',
                  fontSize: 12,
                }}
                status="unchecked"
                color="#e70065"
                style={{
                  width: 350,
                  height: 22,
                  alignSelf: 'center',
                }}
              />
            </View>
          </View>
        </View>
        <View style={introStyles.slide2}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-start',
                backgroundColor: 'white',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 100,
                  width: 350,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/GoVerify_Logo.png')}
              />
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>1</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#dcdcdc',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>2</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#dcdcdc',
                    borderRadius: 8,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>3</Title>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 5,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  width: 350,
                  height: 40,
                  backgroundColor: '#dcdcdc',
                  borderRadius: 8,
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                <Title style={{alignSelf: 'center', color: 'black'}}>
                  United States
                </Title>
              </View>
              <Caption style={{alignSelf: 'center'}}>
                Select Issuing Country
              </Caption>

              <Title
                style={{
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                Select Your Document
              </Title>

              <Caption
                style={{
                  alignSelf: 'center',
                  width: 250,
                  textAlign: 'center',
                  marginBottom: 30,
                }}>
                It must be an official photo ID
              </Caption>
              <List.Item
                onPress={() => {}}
                key={UUID.generate()}
                style={{height: 50, width: 350}}
                title="Passport"
                description=""
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon="passport"
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
              <List.Item
                onPress={() => {}}
                key={UUID.generate()}
                style={{height: 50, width: 350}}
                title="National Identity Card"
                description=""
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon="card-account-details-outline"
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
              <List.Item
                onPress={() => {}}
                key={UUID.generate()}
                style={{height: 50, width: 350}}
                title="Driver's License"
                description=""
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon="car"
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
              <List.Item
                onPress={() => {}}
                key={UUID.generate()}
                style={{height: 50, width: 350}}
                title="STA Application"
                description=""
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon="shield-account-outline"
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
            </View>
          </View>
        </View>
        <View style={introStyles.slide2}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-start',
                backgroundColor: 'white',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 100,
                  width: 350,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/GoVerify_Logo.png')}
              />
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>1</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#dcdcdc',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>2</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#dcdcdc',
                    borderRadius: 8,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>3</Title>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <Title
                style={{
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                Select Your Selfie
              </Title>
            </View>

            <View
              style={{
                flex: 4,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <List.Item
                onPress={() => {}}
                key={UUID.generate()}
                style={{height: 60, width: 350}}
                title="Live Photo"
                description=""
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon="camera-outline"
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
              <Caption style={{width: 320, marginBottom: 30}}>
                Take a live photo to verify your identity with the selected
                document.
              </Caption>
              <List.Item
                onPress={() => {}}
                key={UUID.generate()}
                style={{height: 60, width: 350}}
                title="Live Video"
                description=""
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon="video-outline"
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
              <Caption style={{width: 320, marginBottom: 30}}>
                Take a live video to verify your identity with the selected
                document.
              </Caption>
            </View>
          </View>
        </View>
        <View style={introStyles.slide2}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <Title
                style={{
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                Upload Your Document
              </Title>
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>1</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>2</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#dcdcdc',
                    borderRadius: 8,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>3</Title>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 4,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 100,
                  width: 450,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/DriversLicense.png')}
              />
              <Caption>Frame your document</Caption>
            </View>

            <View
              style={{
                flex: 1,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 70,
                  width: 70,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/PhotoButton.png')}
              />
            </View>
          </View>
        </View>
        <View style={introStyles.slide2}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <Title
                style={{
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                Take a Selfie
              </Title>
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>1</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    marginRight: 30,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>2</Title>
                </View>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    backgroundColor: '#e70065',
                    borderRadius: 8,
                    justifyContent: 'center',
                  }}>
                  <Title style={{alignSelf: 'center', color: 'white'}}>3</Title>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 4,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 100,
                  width: 400,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/Selfie.png')}
              />
              <Caption>Frame yourself</Caption>
            </View>

            <View
              style={{
                flex: 1,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 70,
                  width: 70,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/PhotoButton.png')}
              />
            </View>
          </View>
        </View>
        <View style={introStyles.slide2}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'space-between',
                backgroundColor: 'white',
                flexDirection: 'column',
              }}>
              <Image
                style={{
                  flex: 1,
                  height: 100,
                  width: 350,
                  resizeMode: 'contain',
                }}
                source={require('./src/assets/images/GoVerify_Logo.png')}
              />
            </View>

            <View
              style={{
                flex: 5,
                alignItems: 'center',
                justifyContent: 'space-around',
                backgroundColor: 'white',
                flexDirection: 'column',
              }}>
              <Title
                style={{
                  alignSelf: 'center',
                  marginTop: 30,
                }}>
                Authentication Complete
              </Title>

              <Image
                style={{
                  resizeMode: 'contain',
                  alignSelf: 'center',
                  height: 300,
                }}
                source={require('./src/assets/images/undraw_Security_on_ff2u.png')}
              />
              <Paragraph
                style={{
                  alignSelf: 'center',
                  width: 250,
                  textAlign: 'center',
                  marginBottom: 30,
                }}>
                Well done! Your authentication is complete
              </Paragraph>
            </View>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                backgroundColor: 'white',
              }}>
              <Title style={{marginBottom: 10}}>
                To continue create an account
              </Title>
              <Button
                style={{
                  alignSelf: 'center',
                  width: 350,
                  marginBottom: 5,
                }}
                mode="contained"
                color="#e70065"
                onPress={async () => {
                  if (preRegisterToken == null) {
                    navigation.navigate('Registration');
                  } else {
                    preRegisterUser = await Api.getPreRegistration(
                      preRegisterToken,
                    );
                    console.log('GO TO COMPLETE SIGN UP');
                    navigation.navigate('Registration');
                  }
                }}>
                CONTINUE
              </Button>
            </View>
          </View>
        </View>
      </Swiper>
    </View>
  );
}

function loginScreen({navigation}) {
  const [username, setUsername] = useState('david_cmatus@yahoo.com');
  const [password, setPassword] = useState('Cris1974');
  const [email, setEmail] = useState('');
  const [dialogVisible, setDialogVisible] = useState(false);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'space-around',
        backgroundColor: 'white',
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        style={{
          flex: 0.8,
          justifyContent: 'space-around',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flex: 0.4,
            alignItems: 'center',
            justifyContent: 'space-around',
            backgroundColor: 'white',
          }}>
          <Image
            style={{flex: 1, height: 100, width: 350, resizeMode: 'contain'}}
            source={require('./src/assets/images/GoVerify_Logo.png')}
          />
        </View>
        <View
          style={{
            flex: 0.3,
            justifyContent: 'space-around',
            backgroundColor: 'white',
          }}>
          <TextInput
            style={{
              height: 40,
              width: 350,
              borderColor: 'gray',
              backgroundColor: 'white',
              borderWidth: 0,
              alignSelf: 'center',
            }}
            placeholder="Username"
            autoCompleteType="username"
            onChangeText={(text) => setUsername(text)}
          />

          <TextInput
            secureTextEntry={true}
            style={{
              height: 40,
              width: 350,
              marginTop: 10,
              marginBottom: 10,
              borderColor: 'gray',
              backgroundColor: 'white',
              borderWidth: 0,
              alignSelf: 'center',
            }}
            autoCompleteType="password"
            placeholder="Password"
            onChangeText={(text) => setPassword(text)}
          />
          <Button
            style={{
              marginTop: 10,
              width: 350,
              alignSelf: 'center',
            }}
            mode="contained"
            color="#e70065"
            onPress={async () => {
              if (username.length > 0 && password.length > 0) {
                await Api.login(username, password);
                userData = await Api.steps();
                if (Api.userPlans().length > 0) {
                  //navigation.navigate('NewPlanWelcome');
                  navigation.navigate('Verification');
                } else {
                  navigation.navigate('NewPlanWelcome');
                }
              } else {
                alert('Please check your credentials');
              }
            }}>
            Login
          </Button>
          <Caption
            style={{
              width: 350,
              alignSelf: 'center',
              textAlign: 'right',
              marginTop: 10,
              fontSize: 15,
            }}
            onPress={async () => {
              setDialogVisible(true);
            }}>
            Reset Password
          </Caption>
        </View>
      </KeyboardAvoidingView>
      <View
        style={{
          flex: 0.2,
          alignItems: 'center',
          justifyContent: 'space-around',
          backgroundColor: 'white',
        }}>
        <Button
          style={{
            marginTop: 10,
            width: 350,
            alignSelf: 'center',
          }}
          mode="text"
          color="gray"
          onPress={async () => {
            navigation.navigate('Registration');
          }}>
          Sign Up
        </Button>
      </View>
      <Portal>
        <Dialog
          visible={dialogVisible}
          onDismiss={() => {
            setDialogVisible(false);
          }}>
          <Dialog.ScrollArea>
            <ScrollView>
              <Dialog.Title>Reset Password</Dialog.Title>
              <Dialog.Content>
                <Text style={{marginBottom: 10}}>
                  Enter your e-mail to reset your password
                </Text>
                <TextInput
                  style={styles.textField}
                  placeholder="E-mail"
                  onChangeText={(text) => setEmail(text)}
                />
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => {
                    setDialogVisible(false);
                  }}>
                  Dismiss
                </Button>
                <Button
                  onPress={() => {
                    if (email.length != 0) {
                      setDialogVisible(false);
                      Api.passwordReset();
                      alert(
                        'Your Password has been reset \nPlease check your e-mail to complete the process',
                      );
                    } else {
                      alert('Invalid E-Mail address, please try again');
                    }
                  }}>
                  Confirm
                </Button>
              </Dialog.Actions>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
      </Portal>
    </View>
  );
}

function registrationScreen({navigation}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [dataEnabled, setDataEnabled] = useState(true);
  const [termAccepted1, setTermAccepted1] = useState(false);
  const [termAccepted2, setTermAccepted2] = useState(false);
  const [termAccepted3, setTermAccepted3] = useState(false);
  const [termAccepted4, setTermAccepted4] = useState(false);
  const [documentsAcceptedTC, setDocumentsAcceptedTC] = useState(false);
  const [documentsAcceptedPP, setDocumentsAcceptedPP] = useState(false);
  const [dialogVisibleTC, setDialogVisibleTC] = useState(false);
  const [dialogVisiblePP, setDialogVisiblePP] = useState(false);

  if (preRegisterUser != null) {
    setUsername(preRegisterUser.userId.email);
    setEmail(preRegisterUser.userId.email);
    setDataEnabled(false);
    setFirstName(preRegisterUser.userId.firstName);
    setLastName(preRegisterUser.userId.lastName);
  }

  console.log('SIGN UP');
  console.log(
    preRegisterUser == null
      ? 'No PreRegisterUser'
      : 'PreRegisterUser: ' + preRegisterUser,
  );

  return (
    <KeyboardAvoidingView
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          flexDirection: 'column',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 5,
          alignItems: 'center',
          justifyContent: 'space-around',
          backgroundColor: 'white',
          flexDirection: 'column',
        }}>
        <Title
          style={{
            alignSelf: 'center',
            marginTop: 30,
          }}>
          Registration
        </Title>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={{
            flex: 1,
            backgroundColor: 'Colors.lighter',
            width: 400,
            marginBottom: 10,
          }}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                placeholder={
                  preRegisterUser == null
                    ? 'Username (required)'
                    : preRegisterUser.userId.email
                }
                editable={dataEnabled}
                onChangeText={(text) => setUsername(text)}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                placeholder={
                  preRegisterUser == null
                    ? 'First Name (required)'
                    : preRegisterUser.userId.firstName
                }
                editable={dataEnabled}
                onChangeText={(text) => setFirstName(text)}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                placeholder={
                  preRegisterUser == null
                    ? 'Last Name (required)'
                    : preRegisterUser.userId.lastName
                }
                editable={dataEnabled}
                onChangeText={(text) => setLastName(text)}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                secureTextEntry={true}
                style={styles.textField}
                placeholder="Password (required)"
                onChangeText={(text) => setPassword(text)}
              />
            </View>

            <View style={styles.sectionContainer}>
              <TextInput
                secureTextEntry={true}
                style={styles.textField}
                placeholder="Confirm Password (required)"
                onChangeText={(text) => setPassword2(text)}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput style={styles.textField} placeholder="Mobile Phone" />
            </View>

            <View style={styles.sectionContainer}>
              <TextInput style={styles.textField} placeholder="Address" />
            </View>
          </View>
        </ScrollView>
      </View>

      <View
        style={{
          flex: 1,

          backgroundColor: 'white',
        }}>
        <Button
          style={{
            alignSelf: 'center',
            width: 350,
            marginBottom: 5,
          }}
          mode="contained"
          color="#e70065"
          onPress={async () => {
            if (!documentsAcceptedTC || !documentsAcceptedPP) {
              alert(
                'To continue your registration and use our service, you must agree to all the terms and conditions',
              );
            } else if (password === password2 && password.length > 4) {
              await Api.register(username, password);
              var onFidoNew = await OnFidoApi.createOnFido(
                firstName,
                lastName,
                email,
              );

              console.log('ON FIDO NEW');
              console.log(onFidoNew);
              console.log(onFidoNew.id);

              await Api.setOnFido(onFidoNew.id);

              if (preRegisterUser == null) {
                var newUserId = await Api.getAuthUserId();
                await Api.createIndividual(newUserId, firstName, lastName);
              } else {
                await Api.updateIndividual(firstName, lastName);
              }

              if (Api.userToken().length > 4) {
                alert('Registration successful');
                navigation.navigate('RegistrationCompleted');
              } else {
                if (result === 200) {
                  alert('Registration successful');
                  navigation.navigate('RegistrationCompleted');
                } else if (result === 500) {
                  alert('User already exists');
                } else {
                  alert('Registration Failed');
                }
              }
            } else {
              alert('Please review your password.');
            }
          }}>
          SIGN UP
        </Button>
        <Checkbox.Item
          label="Accept Terms of Use"
          labelStyle={{
            color: 'black',
            fontSize: 12,
          }}
          status={documentsAcceptedTC ? 'checked' : 'unchecked'}
          color="#e70065"
          style={{
            width: 350,
            height: 22,
            alignSelf: 'center',
          }}
          onPress={() => {
            setDialogVisibleTC(true);
          }}
        />
        <Checkbox.Item
          label="Accept Privacy Policy"
          labelStyle={{
            color: 'black',
            fontSize: 12,
          }}
          status={documentsAcceptedPP ? 'checked' : 'unchecked'}
          color="#e70065"
          style={{
            width: 350,
            height: 22,
            alignSelf: 'center',
          }}
          onPress={() => {
            setDialogVisiblePP(true);
          }}
        />
      </View>
      <Portal>
        <Dialog
          visible={dialogVisibleTC}
          onDismiss={() => {
            setDialogVisibleTC(false);
          }}>
          <Dialog.ScrollArea>
            <ScrollView>
              <Dialog.Title>Terms & Conditions</Dialog.Title>
              <Dialog.Content>
                <Card style={{marginBottom: 10}}>
                  <Card.Title
                    title={Texts.termsConditions1()[0]}
                    subtitle={Texts.termsConditions1()[1]}
                  />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions1()[2]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions1()[3]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted2}
                      onPress={() => {
                        setTermAccepted2(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
                <Card style={{marginBottom: 10}}>
                  <Card.Title
                    title={Texts.termsConditions2()[0]}
                    subtitle={Texts.termsConditions2()[1]}
                  />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions2()[2]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions2()[3]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted3}
                      onPress={() => {
                        setTermAccepted3(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
                <Card style={{marginBottom: 10}}>
                  <Card.Title
                    title={Texts.termsConditions3()[0]}
                    subtitle={Texts.termsConditions3()[1]}
                  />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions3()[2]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions3()[3]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted4}
                      onPress={() => {
                        setTermAccepted4(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => {
                    setDialogVisibleTC(false);
                    if (termAccepted2 && termAccepted3 && termAccepted4) {
                      setDocumentsAcceptedTC(true);
                    } else {
                      alert(
                        'To continue your registration and use our service, you must agree to all the terms and conditions',
                      );
                    }
                  }}>
                  Done
                </Button>
              </Dialog.Actions>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
        <Dialog
          visible={dialogVisiblePP}
          onDismiss={() => {
            setDialogVisiblePP(false);
          }}>
          <Dialog.ScrollArea>
            <ScrollView>
              <Dialog.Title>Privacy Policy</Dialog.Title>
              <Dialog.Content>
                <Card style={{marginBottom: 10}}>
                  <Card.Title title="Welcome" subtitle="" />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.privacyPolicy()[0]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.privacyPolicy()[1]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted1}
                      onPress={() => {
                        setTermAccepted1(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => {
                    setDialogVisiblePP(false);
                    if (termAccepted1) {
                      setDocumentsAcceptedPP(true);
                    } else {
                      alert(
                        'To continue your registration and use our service, you must agree to our Privacy Policy',
                      );
                    }
                  }}>
                  Done
                </Button>
              </Dialog.Actions>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
      </Portal>
    </KeyboardAvoidingView>
  );
}

function registrationCompletedScreen({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: 'white',
            flexDirection: 'column',
          }}>
          <Image
            style={{
              flex: 1,
              height: 100,
              width: 350,
              resizeMode: 'contain',
            }}
            source={require('./src/assets/images/GoVerify_Logo.png')}
          />
        </View>

        <View
          style={{
            flex: 5,
            alignItems: 'center',
            justifyContent: 'space-around',
            backgroundColor: 'white',
            flexDirection: 'column',
          }}>
          <Title
            style={{
              alignSelf: 'center',
              marginTop: 30,
            }}>
            Confirm Your E-Mail
          </Title>

          <Image
            style={{
              resizeMode: 'contain',
              alignSelf: 'center',
              height: 300,
            }}
            source={require('./src/assets/images/undraw_Mail_sent_qwwx.png')}
          />
          <Paragraph
            style={{
              alignSelf: 'center',
              width: 250,
              textAlign: 'center',
              marginBottom: 30,
            }}>
            Please follow the link we've sent to your inbox to complete the
            registration
          </Paragraph>
        </View>

        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
          }}>
          <Button
            style={{
              alignSelf: 'center',
              width: 350,
              marginBottom: 5,
            }}
            mode="contained"
            color="#e70065"
            onPress={async () => {
              navigation.navigate('Login');
            }}>
            DONE
          </Button>
        </View>
      </View>
    </View>
  );
}

function verificationScreen({navigation}) {
  const [menuVisible, setMenuVisible] = useState(false);
  var planCount = 0;

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          height: 80,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: 'white',
        }}>
        <Image
          style={{flex: 1, resizeMode: 'center', height: '120%'}}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
        <Menu
          visible={menuVisible}
          onDismiss={() => {
            setMenuVisible(false);
          }}
          anchor={
            <IconButton
              icon="menu"
              size={40}
              color={'#e70065'}
              onPress={() => {
                setMenuVisible(true);
              }}
            />
          }>
          <Menu.Item
            key={UUID.generate()}
            onPress={() => {
              setMenuVisible(false);
              navigation.navigate('Profile');
            }}
            title="Profile"
            icon="account-outline"
          />
          <Divider />
          <Menu.Item
            key={UUID.generate()}
            onPress={() => {
              setMenuVisible(false);
              Alert.alert(
                'Password Reset',
                'Please confirm your password reset',
                [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'Confirm', onPress: () => Api.passwordReset()},
                ],
                {cancelable: false},
              );
            }}
            title="Reset Password"
            icon="lock-reset"
          />
          <Divider />
          <Menu.Item
            key={UUID.generate()}
            onPress={() => {
              setMenuVisible(false);
              navigation.navigate('About');
            }}
            title="About GoVerify"
            icon="help-circle-outline"
          />
        </Menu>
      </View>
      <Divider />
      <List.Item
        key={UUID.generate()}
        title={Api.userData().fullName}
        description={Api.userData().userEmail}
        left={(props) => (
          <Avatar.Text
            {...props}
            style={{backgroundColor: '#dcdcdc'}}
            label={Api.userInitials()}
          />
        )}
        right={(props) => (
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <IconButton
              icon="account-box-outline"
              color="white"
              onPress={() => {} /**navigation.navigate('Evero')*/}
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
            />
            <Text style={{fontSize: 10}}>My È-Vero ID</Text>
          </View>
        )}
      />
      <Divider />
      <ScrollView>
        <List.AccordionGroup>
          {Api.userPlans().map((plans, index) => (
            <List.Accordion
              key={UUID.generate()}
              title={plans.name}
              description={plans.plan.name}
              id={index + 1}
              left={(props) => (
                <List.Icon {...props} icon="account-circle-outline" />
              )}>
              {plans.verificationTypes.map((verType, index2) => (
                <>
                  <Divider key={UUID.generate()} />

                  {getVerificationItem(verType)}
                </>
              ))}
              <List.Item
                key={UUID.generate()}
                onPress={() => {
                  alert('PLAN LAUNCHED');
                }}
                style={{height: 80}}
                title={'Launch Plan Verification'}
                left={(props) => (
                  <List.Icon
                    style={{
                      backgroundColor: '#e70065',
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: '#e70065',
                    }}
                    color="white"
                    icon={'check'}
                  />
                )}
                right={(props) => (
                  <List.Icon
                    color="#e70065"
                    icon="chevron-right-circle-outline"
                  />
                )}
              />
            </List.Accordion>
          ))}
          <List.Item
            onPress={() => {
              navigation.navigate('NewPlanWelcome');
            }}
            key={UUID.generate()}
            style={{height: 80}}
            title="Add a new plan"
            left={(props) => (
              <List.Icon
                style={{
                  backgroundColor: 'white',
                  borderRadius: 8,
                  borderWidth: 1,
                  borderColor: 'white',
                }}
                color="#e70065"
                icon="file-check-outline"
              />
            )}
            right={(props) => (
              <List.Icon color="#e70065" icon="plus-circle-outline" />
            )}
          />
        </List.AccordionGroup>
      </ScrollView>
    </View>
  );
}

function newPlanWelcomeScreen({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-start',
          backgroundColor: 'white',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Title
          style={{
            alignSelf: 'center',
          }}>
          Select a Plan to Start
        </Title>
      </View>

      <View
        style={{
          flex: 3,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          style={{
            resizeMode: 'contain',
            alignSelf: 'center',
            height: 300,
          }}
          source={require('./src/assets/images/undraw_empty_xct9.png')}
        />
      </View>

      <View
        style={{
          flex: 2,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: 10,
        }}>
        <List.Item
          onPress={() => {
            newPlanName = 'Document Verification';
            navigation.navigate('NewPlan');
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="Document Verification"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="file-check-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <List.Item
          onPress={() => {
            newPlanName = 'Biometric Verification';
            selectSelfie = true;
            navigation.navigate('NewPlan');
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="Biometric Verification"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="card-account-details-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <List.Item
          onPress={() => {
            newPlanName = 'Account Authentication';
            selectSelfie = true;
            navigation.navigate('NewPlan');
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="Account Authentication"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="account-check-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
      </View>
    </View>
  );
}

function newPlanScreen({navigation}) {
  const [termAccepted1, setTermAccepted1] = useState(false);
  const [termAccepted2, setTermAccepted2] = useState(false);
  const [termAccepted3, setTermAccepted3] = useState(false);
  const [termAccepted4, setTermAccepted4] = useState(false);
  const [documentsAcceptedTC, setDocumentsAcceptedTC] = useState(false);
  const [documentsAcceptedPP, setDocumentsAcceptedPP] = useState(false);
  const [dialogVisibleTC, setDialogVisibleTC] = useState(false);
  const [dialogVisiblePP, setDialogVisiblePP] = useState(false);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          flexDirection: 'column',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 5,
          alignItems: 'center',
          justifyContent: 'space-around',
          backgroundColor: 'white',
          flexDirection: 'column',
        }}>
        <Title
          style={{
            alignSelf: 'center',
            marginTop: 30,
          }}>
          {newPlanName}
        </Title>

        <Image
          style={{
            resizeMode: 'contain',
            alignSelf: 'center',
            height: 300,
          }}
          source={require('./src/assets/images/undraw_hiring_cyhs.png')}
        />
        <Paragraph
          style={{
            alignSelf: 'center',
            width: 250,
            textAlign: 'center',
            marginBottom: 30,
          }}>
          We can verify and authenticate your identity in less than 20 seconds
        </Paragraph>
      </View>

      <View
        style={{
          flex: 1,

          backgroundColor: 'white',
        }}>
        <Button
          style={{
            alignSelf: 'center',
            width: 350,
            marginBottom: 5,
          }}
          mode="contained"
          color="#e70065"
          onPress={async () => {
            if (documentsAcceptedTC && documentsAcceptedPP) {
              navigation.navigate('DocumentSelection');
            } else {
              alert(
                'To continue your registration and use our service, you must agree to all the terms and conditions',
              );
            }
          }}>
          START
        </Button>
        <Checkbox.Item
          label="Accept Terms of Use"
          labelStyle={{
            color: 'black',
            fontSize: 12,
          }}
          status={documentsAcceptedTC ? 'checked' : 'unchecked'}
          color="#e70065"
          style={{
            width: 350,
            height: 22,
            alignSelf: 'center',
          }}
          onPress={() => {
            setDialogVisibleTC(true);
          }}
        />
        <Checkbox.Item
          label="Accept Privacy Policy"
          labelStyle={{
            color: 'black',
            fontSize: 12,
          }}
          status={documentsAcceptedPP ? 'checked' : 'unchecked'}
          color="#e70065"
          style={{
            width: 350,
            height: 22,
            alignSelf: 'center',
          }}
          onPress={() => {
            setDialogVisiblePP(true);
          }}
        />
      </View>
      <Portal>
        <Dialog
          visible={dialogVisibleTC}
          onDismiss={() => {
            setDialogVisibleTC(false);
          }}>
          <Dialog.ScrollArea>
            <ScrollView>
              <Dialog.Title>Terms & Conditions</Dialog.Title>
              <Dialog.Content>
                <Card style={{marginBottom: 10}}>
                  <Card.Title
                    title={Texts.termsConditions1()[0]}
                    subtitle={Texts.termsConditions1()[1]}
                  />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions1()[2]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions1()[3]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted2}
                      onPress={() => {
                        setTermAccepted2(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
                <Card style={{marginBottom: 10}}>
                  <Card.Title
                    title={Texts.termsConditions2()[0]}
                    subtitle={Texts.termsConditions2()[1]}
                  />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions2()[2]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions2()[3]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted3}
                      onPress={() => {
                        setTermAccepted3(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
                <Card style={{marginBottom: 10}}>
                  <Card.Title
                    title={Texts.termsConditions3()[0]}
                    subtitle={Texts.termsConditions3()[1]}
                  />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions3()[2]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.termsConditions3()[3]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted4}
                      onPress={() => {
                        setTermAccepted4(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => {
                    setDialogVisibleTC(false);
                    if (termAccepted2 && termAccepted3 && termAccepted4) {
                      setDocumentsAcceptedTC(true);
                    } else {
                      alert(
                        'To continue your registration and use our service, you must agree to all the terms and conditions',
                      );
                    }
                  }}>
                  Done
                </Button>
              </Dialog.Actions>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
        <Dialog
          visible={dialogVisiblePP}
          onDismiss={() => {
            setDialogVisiblePP(false);
          }}>
          <Dialog.ScrollArea>
            <ScrollView>
              <Dialog.Title>Privacy Policy</Dialog.Title>
              <Dialog.Content>
                <Card style={{marginBottom: 10}}>
                  <Card.Title title="Welcome" subtitle="" />
                  <Card.Content>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.privacyPolicy()[0]}
                    </Paragraph>
                    <Paragraph style={{textAlign: 'justify'}}>
                      {Texts.privacyPolicy()[1]}
                    </Paragraph>
                  </Card.Content>
                  <Card.Actions style={{alignContent: 'flex-end'}}>
                    <Button
                      disabled={termAccepted1}
                      onPress={() => {
                        setTermAccepted1(true);
                      }}>
                      Agree
                    </Button>
                  </Card.Actions>
                </Card>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => {
                    setDialogVisiblePP(false);
                    if (termAccepted1) {
                      setDocumentsAcceptedPP(true);
                    } else {
                      alert(
                        'To continue your registration and use our service, you must agree to our Privacy Policy',
                      );
                    }
                  }}>
                  Done
                </Button>
              </Dialog.Actions>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
      </Portal>
    </View>
  );
}

function documentSelectionScreen({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-start',
          backgroundColor: 'white',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: 50,
              height: 50,
              backgroundColor: '#e70065',
              borderRadius: 8,
              marginRight: 30,
              justifyContent: 'center',
            }}>
            <Title style={{alignSelf: 'center', color: 'white'}}>1</Title>
          </View>
          <View
            style={{
              width: 50,
              height: 50,
              backgroundColor: '#dcdcdc',
              borderRadius: 8,
              marginRight: 30,
              justifyContent: 'center',
            }}>
            <Title style={{alignSelf: 'center', color: 'white'}}>2</Title>
          </View>
          <View
            style={{
              width: 50,
              height: 50,
              backgroundColor: '#dcdcdc',
              borderRadius: 8,
              justifyContent: 'center',
            }}>
            <Title style={{alignSelf: 'center', color: 'white'}}>3</Title>
          </View>
        </View>
      </View>

      <View
        style={{
          flex: 5,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: 350,
            height: 40,
            backgroundColor: '#dcdcdc',
            borderRadius: 8,
            justifyContent: 'center',
            alignSelf: 'center',
          }}>
          <Title style={{alignSelf: 'center', color: 'black'}}>
            United States
          </Title>
        </View>
        <Caption style={{alignSelf: 'center'}}>Select Issuing Country</Caption>

        <Title
          style={{
            alignSelf: 'center',
            marginTop: 30,
          }}>
          Select Your Document
        </Title>

        <Caption
          style={{
            alignSelf: 'center',
            width: 250,
            textAlign: 'center',
            marginBottom: 30,
          }}>
          It must be an official photo ID
        </Caption>
        <List.Item
          onPress={async () => {
            documentSelected = 'passport';
            if (selectSelfie) {
              navigation.navigate('SelfieSelection');
            } else {
              await OnFidoApi.onfidoPass(
                documentSelected,
                'USA',
                userData.onfidoId,
                false,
                false,
              );
              navigation.navigate('Payment');
            }
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="Passport"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="passport"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <List.Item
          onPress={async () => {
            documentSelected = 'nationalIdentityCard';
            if (selectSelfie) {
              navigation.navigate('SelfieSelection');
            } else {
              await OnFidoApi.onfidoPass(
                documentSelected,
                'USA',
                userData.onfidoId,
                false,
                false,
              );
              navigation.navigate('Payment');
            }
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="National Identity Card"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="card-account-details-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <List.Item
          onPress={async () => {
            documentSelected = 'drivingLicence';
            if (selectSelfie) {
              navigation.navigate('SelfieSelection');
            } else {
              await OnFidoApi.onfidoPass(
                documentSelected,
                'USA',
                userData.onfidoId,
                false,
                false,
              );
              navigation.navigate('Payment');
            }
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="Driver's Licence"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="car"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <List.Item
          onPress={async () => {
            documentSelected = 'sta';
            if (selectSelfie) {
              navigation.navigate('SelfieSelection');
            } else {
              await OnFidoApi.onfidoPass(
                documentSelected,
                'USA',
                userData.onfidoId,
                false,
                false,
              );
              navigation.navigate('Payment');
            }
          }}
          key={UUID.generate()}
          style={{height: 50, width: 350}}
          title="STA Application"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="shield-account-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
      </View>
    </View>
  );
}

function selfieSelectionScreen({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-start',
          backgroundColor: 'white',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: 50,
              height: 50,
              backgroundColor: '#e70065',
              borderRadius: 8,
              marginRight: 30,
              justifyContent: 'center',
            }}>
            <Title style={{alignSelf: 'center', color: 'white'}}>1</Title>
          </View>
          <View
            style={{
              width: 50,
              height: 50,
              backgroundColor: '#e70065',
              borderRadius: 8,
              marginRight: 30,
              justifyContent: 'center',
            }}>
            <Title style={{alignSelf: 'center', color: 'white'}}>2</Title>
          </View>
          <View
            style={{
              width: 50,
              height: 50,
              backgroundColor: '#dcdcdc',
              borderRadius: 8,
              justifyContent: 'center',
            }}>
            <Title style={{alignSelf: 'center', color: 'white'}}>3</Title>
          </View>
        </View>
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Title
          style={{
            alignSelf: 'center',
            marginTop: 30,
          }}>
          Select Your Selfie
        </Title>
      </View>

      <View
        style={{
          flex: 4,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <List.Item
          onPress={async () => {
            await OnFidoApi.onfidoPass(
              documentSelected,
              'USA',
              userData.onfidoId,
              true,
              true,
            );
            navigation.navigate('Payment');
          }}
          key={UUID.generate()}
          style={{height: 60, width: 350}}
          title="Live Photo"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="camera-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <Caption style={{width: 320, marginBottom: 30}}>
          Take a live photo to verify your identity with the selected document.
        </Caption>
        <List.Item
          onPress={async () => {
            await OnFidoApi.onfidoPass(
              documentSelected,
              'USA',
              userData.onfidoId,
              true,
              false,
            );
            navigation.navigate('Payment');
          }}
          key={UUID.generate()}
          style={{height: 60, width: 350}}
          title="Live Video"
          description=""
          left={(props) => (
            <List.Icon
              style={{
                backgroundColor: '#e70065',
                borderRadius: 8,
                borderWidth: 1,
                borderColor: '#e70065',
              }}
              color="white"
              icon="video-outline"
            />
          )}
          right={(props) => (
            <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
          )}
        />
        <Caption style={{width: 320, marginBottom: 30}}>
          Take a live video to verify your identity with the selected document.
        </Caption>
      </View>
    </View>
  );
}

function paymentScreen({navigation}) {
  var cardData;

  var planName = newPlanName;
  var planDescription = '';
  var price = planPrice;
  var planCurrency = currency;

  //var verificationsToPurchase = showObject(verificationsArray.verifications);

  //console.log(verificationsToPurchase);

  const [menuVisible, setMenuVisible] = useState(false);
  //const [newPlanName, setNewPlanName] = useState('New Plan');

  const _onChange = (formData) => {
    cardData = formData;
    //console.log(JSON.stringify(cardData, null, " "));
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-start',
          backgroundColor: 'white',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Title>{planName}</Title>
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'stretch',
          justifyContent: 'center',
          backgroundColor: 'white',
          flexDirection: 'row',
          width: 350,
        }}>
        <View style={{flex: 1}}>
          <Subheading style={{textAlign: 'left'}}>{'Summary'}</Subheading>
          <Caption style={{textAlign: 'left'}}>{documentSelected}</Caption>
          <Caption style={{textAlign: 'left'}}>
            {selectSelfie ? (photoSelfie ? 'Live Photo' : 'Live Video') : ''}
          </Caption>
        </View>
        <View style={{flex: 1}}>
          <Subheading style={{textAlign: 'right'}}>
            {'Total Price ' + planCurrency + ' $' + price}
          </Subheading>
        </View>
      </View>
      <View
        style={{
          flex: 4,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <CreditCardInput
          onChange={_onChange}
          requiresName
          requiresCVC
          requiresPostalCode
          allowScroll
          cardImageFront={require('./src/assets/images/goverifyCardFront.png')}
          cardImageBack={require('./src/assets/images/goverifyCardBack.png')}
        />
        <Button
          style={{
            marginTop: 20,
            height: 40,
            width: 350,
            //alignSelf: 'center',
            marginBottom: 20,
            bottom: 0,
            position: 'absolute',
          }}
          mode="contained"
          color="#e70065"
          onPress={async () =>
            {var paymentIntent = await StripeApi.processPayment(
              cardData,
              price,
              planCurrency,
              "PLAN",
              newPlanName,
            );
            await Api.createNewPlan(
              "plan",
              documentSelected,
              price,
              newPlanName,
              cardData.values.number,
              paymentIntent.id,
            );
            userData = await Api.steps();
            navigation.navigate('Verification');

            }
          }>
          CHECKOUT
        </Button>
      </View>
    </View>
  );
}

function profileScreen({navigation}) {
  const [dataEnabled, setDataEnabled] = useState(false);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-start',
          backgroundColor: 'white',
        }}>
        <Image
          style={{
            flex: 1,
            height: 100,
            width: 350,
            resizeMode: 'contain',
          }}
          source={require('./src/assets/images/GoVerify_Logo.png')}
        />
      </View>

      <View
        style={{
          flex: 1,
          alignItems: 'flex-start',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Title
          style={{
            alignSelf: 'center',
          }}>
          User Profile
        </Title>
      </View>

      <View
        style={{
          flex: 4,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={{
            flex: 1,
            backgroundColor: 'Colors.lighter',
            width: 400,
            marginBottom: 10,
          }}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                value={userData.userId.email}
                editable={dataEnabled}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                value={userData.firstName}
                editable={dataEnabled}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                value={userData.lastName1}
                editable={dataEnabled}
              />
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                editable={dataEnabled}
                placeholder="Mobile Phone"
              />
            </View>

            <View style={styles.sectionContainer}>
              <TextInput
                style={styles.textField}
                editable={dataEnabled}
                placeholder="Address"
              />
            </View>
          </View>
        </ScrollView>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Button
          style={{
            alignSelf: 'center',
            width: 350,
          }}
          mode="contained"
          color="#e70065"
          onPress={async () => {
            navigation.navigate('Verification');
          }}>
          BACK
        </Button>
      </View>
    </View>
  );
}

function aboutScreen({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      <View
        style={{flex: 5, backgroundColor: 'white', justifyContent: 'center'}}>
        <Image
          style={{alignSelf: 'stretch', height: 300, width: 300}}
          source={require('./src/assets/images/Head_Logo_Text.png')}
        />
      </View>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Title
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            width: 350,
          }}>
          GoVerify Inc. Copyright 2020
        </Title>
        <Title
          style={{
            alignSelf: 'center',
            textAlign: 'center',
            width: 350,
          }}>
          All Rights Reserved
        </Title>
      </View>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Button
          style={{
            alignSelf: 'center',
            width: 350,
            marginTop: 20,
          }}
          mode="contained"
          color="#e70065"
          onPress={async () => {
            navigation.navigate('Verification');
          }}>
          BACK
        </Button>
      </View>
    </View>
  );
}

//Support Functions
function getVerificationItem(verType) {
  const [visible, setVisible] = useState(false);

  var docValue = '';
  var parentId = '';
  var parentValue = '';
  var onFidoType = OnFidoApi.onFidoDocs()[9];
  var docValueAux = '';

  console.log('GET VERIFICATION ITEM ');

  //Get item name from catalog
  Api.catalogs().rows.map((catalog) => {
    if (catalog.catId == 'PLT') {
      catalog.values.map((planItem) => {
        if (planItem.id == verType.id) {
          docValue = planItem.value;
          parentId = planItem.parentKey;

          console.log(parentId);
          console.log(docValue);
        }
      });
    }
  });

  //Get item name from PlanTypes
  Api.allPlanTypes().rows.map((planType) => {
    if (planType.id == verType.id) {
      docValue = planType.verificationType.type.value;
      parentId = planType.planId.id;

      console.log(parentId);
      console.log(docValue);
    }
  });

  //Get parent from catalog
  Api.catalogs().rows.map((catalog) => {
    if (catalog.catId == 'VFY') {
      catalog.values.map((verifierItem) => {
        if (verifierItem.id == parentId) {
          parentValue = verifierItem.key;
        }
      });
    }
  });

  //set onfido types
  docValueAux = docValue.toLowerCase();
  if (parentValue == 'onfido') {
    if (docValueAux.includes('passport')) {
      onFidoType = OnFidoApi.onFidoDocs()[0];
    } else if (docValueAux.includes('driving')) {
      onFidoType = OnFidoApi.onFidoDocs()[1];
    } else if (docValueAux.includes('identity')) {
      onFidoType = OnFidoApi.onFidoDocs()[2];
    } else if (docValueAux.includes('residence')) {
      onFidoType = OnFidoApi.onFidoDocs()[3];
    } else if (docValueAux.includes('visa')) {
      onFidoType = OnFidoApi.onFidoDocs()[4];
    } else if (docValueAux.includes('work')) {
      onFidoType = OnFidoApi.onFidoDocs()[5];
    } else if (docValueAux.includes('sta')) {
      onFidoType = OnFidoApi.onFidoDocs()[6];
    } else if (docValueAux.includes('document')) {
      onFidoType = OnFidoApi.onFidoDocs()[7];
    } else if (docValueAux.includes('facial')) {
      onFidoType = OnFidoApi.onFidoDocs()[8];
    }
  } else {
    if (docValueAux.includes('passport')) {
      onFidoType = OnFidoApi.onFidoDocs()[0];
    } else if (docValueAux.includes('driving')) {
      onFidoType = OnFidoApi.onFidoDocs()[1];
    } else if (docValueAux.includes('identity')) {
      onFidoType = OnFidoApi.onFidoDocs()[2];
    } else if (docValueAux.includes('residence')) {
      onFidoType = OnFidoApi.onFidoDocs()[3];
    } else if (docValueAux.includes('visa')) {
      onFidoType = OnFidoApi.onFidoDocs()[4];
    } else if (docValueAux.includes('work')) {
      onFidoType = OnFidoApi.onFidoDocs()[5];
    } else if (docValueAux.includes('sta')) {
      onFidoType = OnFidoApi.onFidoDocs()[6];
    } else if (docValueAux.includes('document')) {
      onFidoType = OnFidoApi.onFidoDocs()[7];
    } else if (docValueAux.includes('facial')) {
      onFidoType = OnFidoApi.onFidoDocs()[8];
    }
  }

  return (
    <List.Item
      key={UUID.generate()}
      onPress={() => {
        //onfidoPass(onFidoType[1], OnfidoCountryCode.USA);
      }}
      style={{height: 80}}
      title={onFidoType[0]}
      description={docValue}
      left={(props) => (
        <List.Icon
          style={{
            backgroundColor: '#e70065',
            borderRadius: 8,
            borderWidth: 1,
            borderColor: '#e70065',
          }}
          color="white"
          icon={onFidoType[2]}
        />
      )}
      right={(props) => (
        <List.Icon color="#e70065" icon="chevron-right-circle-outline" />
      )}
    />
  );
}

function getAvailablePlans(plan, navigation) {
  return (
    <List.Item
      onPress={() => {
        /**navigation.navigate('New Plan', plan);*/
      }}
      key={UUID.generate()}
      style={{height: 80}}
      title={plan.name}
      description={plan.description}
      left={(props) => (
        <List.Icon
          style={{
            backgroundColor: '#e70065',
            borderRadius: 8,
            borderWidth: 1,
            borderColor: '#e70065',
          }}
          color="white"
          icon="file-check-outline"
        />
      )}
      right={(props) => (
        <List.Icon color="#e70065" icon="plus-circle-outline" />
      )}
    />
  );
}

function showObject(obj) {
  var result = [];
  for (var p in obj) {
    if (obj.hasOwnProperty(p)) {
      result.push(p);
    }
  }
  return result;
}

const App: () => React$Node = () => {
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="Welcome" component={welcomeScreen} />
          <Stack.Screen name="Intro" component={introScreen} />
          <Stack.Screen name="Login" component={loginScreen} />
          <Stack.Screen name="Registration" component={registrationScreen} />
          <Stack.Screen
            name="RegistrationCompleted"
            component={registrationCompletedScreen}
          />
          <Stack.Screen name="Verification" component={verificationScreen} />
          <Stack.Screen
            name="NewPlanWelcome"
            component={newPlanWelcomeScreen}
          />
          <Stack.Screen name="NewPlan" component={newPlanScreen} />
          <Stack.Screen
            name="DocumentSelection"
            component={documentSelectionScreen}
          />
          <Stack.Screen
            name="SelfieSelection"
            component={selfieSelectionScreen}
          />
          <Stack.Screen name="Payment" component={paymentScreen} />
          <Stack.Screen name="Profile" component={profileScreen} />
          <Stack.Screen name="About" component={aboutScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

//Stack Navigator
const Stack = createStackNavigator();

//Get DeepLink Logic
const useMount = (func) => useEffect(() => func(), []);

const useInitialURL = () => {
  const [url, setUrl] = useState(null);
  const [processing, setProcessing] = useState(true);

  useMount(() => {
    const getUrlAsync = async () => {
      // Get the deep link used to open the app
      const initialUrl = await Linking.getInitialURL();

      // The setTimeout is just for testing purpose
      setTimeout(() => {
        setUrl(initialUrl);
        setProcessing(false);
      }, 1000);
    };

    getUrlAsync();
  });

  return {url, processing};
};

//APP Theme
const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#e70075',
    accent: '#dcdcdc',
    background: '#ffffff',
  },
};

//Styles
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  textField: {
    borderColor: 'gray',
    backgroundColor: 'white',
    borderWidth: 0,
    height: 40,
  },
  field: {
    width: 300,
    color: '#449aeb',
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 5,
  },
});

export default App;
